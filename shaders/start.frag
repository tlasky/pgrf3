#version 420
in vec2 texCoord;
in vec3 normal;
in vec3 light;
in vec3 viewDirection;
in vec4 depthTextureCoord;
in vec3 pos3;

uniform int solid;
uniform sampler2D depthTexture;
uniform sampler2D mosaic;
uniform vec3 lightPosition;

uniform int colorMode;
uniform int lightAttenuation;
uniform vec3 cameraPosition;
uniform int lightCutoff;

out vec4 outColor;// output from the fragment shader

void main() {
    vec3 ambient = vec3(0.2);

    float NdotL = max(0.0, dot(normalize(normal), normalize(light)));
    vec3 diffuse = NdotL * vec3(0.7);

    vec3 halfVector = normalize(light + viewDirection);
    float NdotH = max(0.0, dot(normalize(normal), halfVector));
    vec3 specular = vec3(pow(NdotH, 16.0));

    float lightDist = distance(lightPosition, light);
    float constantAttenuation  = 0.1;
    float linearAttenuation    = 0.1;
    float quadraticAttenuation = 0.1;
    float att = 1;
    if (lightAttenuation == 1) {
        att = 1.0 / (
        constantAttenuation + linearAttenuation *
        lightDist + quadraticAttenuation *
        lightDist * lightDist
        );
    }

    vec3 colorIntensity = ambient + att * (diffuse + specular);
    vec3 textureColor = texture(mosaic, texCoord).rgb;

    // "z" hodnota z textury
    // R, G i B složky jsou stejné, protože gl_FragCoord.zzz
    // r -> v light.frag ukládáme gl_FragCoord.zzz, takže jsou všechny hodnoty stejné
    float zLight = texture(depthTexture, depthTextureCoord.xy).r;// Z hodnota ke světlu nejbližšího pixelu na této pozici

    // aktuální hodnota
    float zActual = depthTextureCoord.z;

    // 0.001 - bias na ostranění tzv. akné
    // lze vyzkoušet různé hodnoty 0.01, 0.001, 0.0001, 0.00001
    if (solid == 0) {
        textureColor = vec3(1, 1, 0);
    } else if (solid == 6 || solid == 3 || solid == 1 || solid == 8) {
        // Idk, ať jsou trochu jiný
        textureColor = normalize(normal);
    } else if (solid == 7) {
        // Golden gorden ❤
        textureColor = vec3(0.8, 0.498039, 0.196078);
    }

    bool shade = zActual > zLight + 0.0001;

    if (colorMode == 1 && shade) {
        // Jiný stíny
        textureColor = vec3(1, 0, 0);
    } else if (colorMode == 2) {
        // Normály
        textureColor = normalize(normal);
    } else if (colorMode == 3){
        textureColor = normalize(depthTextureCoord.xyz);
    } else if (colorMode == 4) {
        textureColor = normalize(viewDirection);
    }

    if (lightCutoff == 1) {
        float headlampCut = 0.976;
        float spotEffect = max(dot(normalize(-lightPosition), -normalize(light)), 0);
        if (spotEffect > 0.01) {
            float blend = clamp((spotEffect-headlampCut)/(1-headlampCut), 0.0, 1.0);
            colorIntensity = mix(ambient, ambient + diffuse + specular, blend);
        }
    }

    if (shade) {
        colorIntensity = ambient;
    }

    outColor = vec4(colorIntensity * textureColor, 1.0);
}
