#version 420
in vec3 inPosition;// input from the vertex buffer

uniform mat4 view;
uniform mat4 projection;

uniform int solid;
uniform float time;

const float PI = 3.14159;


vec3 getSphere(vec2 pos, float r, float a, float b) {
    float az = pos.x * PI;// souřadnice X je v <-1; 1> a chceme v rozsahu <-PI; PI>
    float ze = pos.y * PI / 2;// souřadnice Y je v <-1; 1> a chceme v rozsahu <-PI/2; PI/2>

    float x = r * cos(az) * cos(ze);
    float y = a * r * sin(az) * cos(ze);
    float z = b * r * sin(ze);

    return vec3(x, y, z);
}

vec3 getSine(vec2 pos) {
    float t = sin(time);
    return vec3(pos.x, pos.y, sin(pos.x * 5) * t / 5);
}

vec3 getPlane(vec2 pos) {
    return vec3(pos, -1);
}

vec3 getBoobs(vec2 pos) {
    // Tohle těleso by se mělo počítat za 2.
    int m = 1000;
    int n = 4;
    float o = 0.1;

    float x = pos.x * 10;
    float y = pos.y * 10;
    float z =
    exp(-pow(pow(x - n, 2) + pow(y - n, 2), 2) / m) +
    exp(-pow(pow(x + n, 2) + pow(y + n, 2), 2) / m) +
    o * exp(-pow(pow(x + n, 2) + pow(y + n, 2), 2)) +
    o * exp(-pow(pow(x - n, 2) + pow(y - n, 2), 2));

    return vec3(x / 10, y / 10, z);
}

vec3 getCylinder(vec2 pos) {
    float az = pos.x * PI;
    float r = 1;

    float x = r*cos(az);
    float y = r*sin(az);
    float z = pos.y * 2;
    return vec3(x, y, z);
}

vec3 getWaves(vec2 pos) {
    // pretty trppy
    float a = abs(sin(time)) * 5;
    float x = pos.x * 10;
    float y = pos.y * 10;
    float z = sin(sqrt(a * pow(x, 2)  + a * pow(y, 2)));
    return vec3(x / 10, y / 10, z / 10);
}

mat3 rotation3dZ(float angle) {
    // https://github.com/dmnsgn/glsl-rotate
    // rotation-3d-*osa*.glsl

    float s = sin(angle);
    float c = cos(angle);

    return mat3(
    c, s, 0.0,
    -s, c, 0.0,
    0.0, 0.0, 1.0
    );
}

mat3 rotation3dY(float angle) {
    float s = sin(angle);
    float c = cos(angle);

    return mat3(
    c, 0.0, -s,
    0.0, 1.0, 0.0,
    s, 0.0, c
    );
}

mat3 rotation3dX(float angle) {
    float s = sin(angle);
    float c = cos(angle);

    return mat3(
    1.0, 0.0, 0.0,
    0.0, c, s,
    0.0, -s, c
    );
}

void main() {
    vec2 position = inPosition.xy * 2 - 1;

    vec3 pos3;
    float scale = 1;
    vec3 move = vec3(0);
    if (solid == 0){
        // Nic, jinak to bude svítit dovnitř objektu
    } else if (solid == 1) {
        move = vec3(0, 0, -2);

        float r = 1;
        float a = 2;
        float b = 0.5;
        pos3 = getSphere(position, r, a, b) * rotation3dZ(time);
    } else if (solid == 2) {
        scale = 3;

        pos3 = getPlane(position);
    } else if (solid == 3) {
        scale = 3;
        move = vec3(6, 0, -3);

        pos3 = getBoobs(position);
    } else if (solid == 4) {
        scale = 3;
        move = vec3(0, -6, -3);

        pos3 = getSine(position);
    } else if (solid == 5) {
        scale = 2;
        move = vec3(-6, 0, 0);

        pos3 = getCylinder(position);
    } else if (solid == 6) {
        scale = 3;
        move = vec3(0, 6, -3);

        pos3 = getWaves(position);
    } else if (solid == 7) {
        scale = 0.1;
        move = vec3(6, -6, -3);

        pos3 = inPosition * rotation3dX(-1.5708) * rotation3dZ(time);
    } else if (solid == 8) {
        scale = 0.4;
        move = vec3(-6, -6, 0);

        pos3 = inPosition * rotation3dY(-1.5708) * rotation3dZ(-time);
    }

    pos3 *= scale;
    pos3.x += move.x;
    pos3.y += move.y;
    pos3.z += move.z;

    gl_Position = projection * view * vec4(pos3, 1.0);
}
