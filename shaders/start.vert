#version 420
in vec3 inPosition;// input from the vertex buffer
in vec3 inNormal;

uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightVP;// light view-projection

uniform int solid;
uniform vec3 lightPosition;
uniform vec3 eyePosition;

uniform float time;

out vec2 texCoord;
out vec3 normal;
out vec3 light;
out vec3 viewDirection;
out vec4 depthTextureCoord;
out vec3 pos3;

const float PI = 3.1415;
const float E = 2.71828;

vec3 getSphere(vec2 pos, float r, float a, float b) {
    float az = pos.x * PI;// souřadnice X je v <-1; 1> a chceme v rozsahu <-PI; PI>
    float ze = pos.y * PI / 2;// souřadnice Y je v <-1; 1> a chceme v rozsahu <-PI/2; PI/2>

    float x = r * cos(az) * cos(ze);
    float y = a * r * sin(az) * cos(ze);
    float z = b * r * sin(ze);

    return vec3(x, y, z);
}

vec3 getSphereNormal(vec2 pos, float r, float a, float b) {
    vec3 u = getSphere(pos + vec2(0.001, 0), r, a, b) - getSphere(pos - vec2(0.001, 0), r, a, b);
    vec3 v = getSphere(pos + vec2(0, 0.001), r, a, b) - getSphere(pos - vec2(0, 0.001), r, a, b);
    return cross(u, v);
}

vec3 getSine(vec2 pos) {
    float t = sin(time);
    return vec3(pos.x, pos.y, sin(pos.x * 5) * t / 5);
}

vec3 getSineNormal(vec2 pos) {
    vec3 u = getSine(pos + vec2(0.001, 0)) - getSine(pos - vec2(0.001, 0));
    vec3 v = getSine(pos + vec2(0, 0.001)) - getSine(pos - vec2(0, 0.001));
    return cross(u, v);
}

vec3 getPlane(vec2 pos) {
    return vec3(pos, -1);
}

vec3 getPlaneNormal(vec2 pos) {
    vec3 u = getPlane(pos + vec2(0.001, 0)) - getPlane(pos - vec2(0.001, 0));
    vec3 v = getPlane(pos + vec2(0, 0.001)) - getPlane(pos - vec2(0, 0.001));
    return cross(u, v);
}

vec3 getBoobs(vec2 pos) {
    // Tohle těleso by se mělo počítat za 2.
    int m = 1000;
    int n = 4;
    float o = 0.1;

    float x = pos.x * 10;
    float y = pos.y * 10;
    float z =
    exp(-pow(pow(x - n, 2) + pow(y - n, 2), 2) / m) +
    exp(-pow(pow(x + n, 2) + pow(y + n, 2), 2) / m) +
    o * exp(-pow(pow(x + n, 2) + pow(y + n, 2), 2)) +
    o * exp(-pow(pow(x - n, 2) + pow(y - n, 2), 2));

    return vec3(x / 10, y / 10, z);
}

vec3 getBoobsNormal(vec2 pos) {
    vec3 u = getBoobs(pos + vec2(0.001, 0)) - getBoobs(pos - vec2(0.001, 0));
    vec3 v = getBoobs(pos + vec2(0, 0.001)) - getBoobs(pos - vec2(0, 0.001));
    return cross(u, v);
}

vec3 getCylinder(vec2 pos) {
    float az = pos.x * PI;
    float r = 1;

    float x = r*cos(az);
    float y = r*sin(az);
    float z = pos.y * 2;
    return vec3(x, y, z);
}

vec3 getCylinderNormal(vec2 pos) {
    vec3 u = getCylinder(pos + vec2(0.001, 0)) - getCylinder(pos - vec2(0.001, 0));
    vec3 v = getCylinder(pos + vec2(0, 0.001)) - getCylinder(pos - vec2(0, 0.001));
    return cross(u, v);
}

vec3 getWaves(vec2 pos) {
    // pretty trppy
    float a = abs(sin(time)) * 5;
    float x = pos.x * 10;
    float y = pos.y * 10;
    float z = sin(sqrt(a * pow(x, 2)  + a * pow(y, 2)));
    return vec3(x / 10, y / 10, z / 10);
}

vec3 getWavesNormal(vec2 pos) {
    vec3 u = getWaves(pos + vec2(0.001, 0)) - getWaves(pos - vec2(0.001, 0));
    vec3 v = getWaves(pos + vec2(0, 0.001)) - getWaves(pos - vec2(0, 0.001));
    return cross(u, v);
}

mat3 rotation3dZ(float angle) {
    // https://github.com/dmnsgn/glsl-rotate
    // rotation-3d-*osa*.glsl

    float s = sin(angle);
    float c = cos(angle);

    return mat3(
    c, s, 0.0,
    -s, c, 0.0,
    0.0, 0.0, 1.0
    );
}

mat3 rotation3dY(float angle) {
    float s = sin(angle);
    float c = cos(angle);

    return mat3(
    c, 0.0, -s,
    0.0, 1.0, 0.0,
    s, 0.0, c
    );
}

mat3 rotation3dX(float angle) {
    float s = sin(angle);
    float c = cos(angle);

    return mat3(
    1.0, 0.0, 0.0,
    0.0, c, s,
    0.0, -s, c
    );
}

void main() {
    texCoord = inPosition.xy;
    vec2 position = inPosition.xy * 2 - 1;

    vec3 pos3;
    float scale = 1;
    vec3 move = vec3(0);
    if (solid == 0){
        move = lightPosition;

        float r = 0.5;
        pos3 = getSphere(position, r, 1, 1);
        normal = getSphereNormal(position, r, 1, 1);
    } else if (solid == 1) {
        move = vec3(0, 0, -2);

        float r = 1;
        float a = 2;
        float b = 0.5;
        pos3 = getSphere(position, r, a, b) * rotation3dZ(time);
        normal = getSphereNormal(position, r, a, b) * rotation3dZ(time);
    } else if (solid == 2) {
        scale = 3;

        pos3 = getPlane(position);
        normal = getPlaneNormal(position);
    } else if (solid == 3) {
        scale = 3;
        move = vec3(6, 0, -3);

        pos3 = getBoobs(position);
        normal = getBoobsNormal(position);
    } else if (solid == 4) {
        scale = 3;
        move = vec3(0, -6, -3);

        pos3 = getSine(position);
        normal = getSineNormal(position);
    } else if (solid == 5) {
        scale = 2;
        move = vec3(-6, 0, 0);

        pos3 = getCylinder(position);
        normal = getCylinderNormal(position);
    } else if (solid == 6) {
        scale = 3;
        move = vec3(0, 6, -3);

        pos3 = getWaves(position);
        normal = getWavesNormal(position);
    } else if (solid == 7) {
        scale = 0.1;
        move = vec3(6, -6, -3);

        pos3 = inPosition * rotation3dX(-1.5708) * rotation3dZ(time);
        normal = inNormal * rotation3dX(-1.5708) * rotation3dZ(time);
    } else if (solid == 8) {
        scale = 0.4;
        move = vec3(-6, -6, -0);

        pos3 = inPosition * rotation3dY(-1.5708) * rotation3dZ(-time);
        normal = inNormal * rotation3dY(-1.5708) * rotation3dZ(-time);
    }

    pos3 *= scale;
    pos3.x += move.x;
    pos3.y += move.y;
    pos3.z += move.z;

    normal *= scale;
    normal.x += normal.x;
    normal.y += normal.y;
    normal.z += normal.z;

    gl_Position = projection * view * vec4(pos3, 1.0);

    light = lightPosition - pos3;
    viewDirection = eyePosition - pos3;

    depthTextureCoord = lightVP * vec4(pos3, 1.0);// získáváme pozici vrcholu tak, jak ten vrchol vidělo (vidí) světlo
    // XY jako souřadnice v obrazovce a Z jako vzdálenost od pozorovatele (v tomto případě světla)
    depthTextureCoord.xyz = depthTextureCoord.xyz / depthTextureCoord.w;// nutná dehomogenizace
    // obrazovka je <-1;1>
    // textura je <0;1>
    depthTextureCoord.xyz = (depthTextureCoord.xyz + 1) / 2;
}
