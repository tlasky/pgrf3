package main;

import lwjglutils.*;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import transforms.*;
import utils.GridFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;

/**
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2019-09-02
 */
public class Renderer extends AbstractRenderer {

  private double oldMx, oldMy;
  private boolean mousePressed;

  private int shaderProgramViewer, shaderProgramLight;
  private OGLBuffers buffers;
  private OGLModelOBJ model1;
  private OGLModelOBJ model2;
  private OGLRenderTarget renderTarget;

  private Camera camera, cameraLight;
  private Mat4 projection;
  private int locView, locProjection, locSolid, locLightPosition, locEyePosition, locLightVP, locTime, locCameraPosition;
  private int locViewLight, locProjectionLight, locSolidLight, locTimeLight, locColorMode, locLightAttenuation, locLightCutoff;
  private OGLTexture2D mosaicTexture;
  private OGLTexture.Viewer viewer;

  private long startTime;
  private double currentTime;

  private final List<Integer> polygonModes = Arrays.asList(
      GL_FILL,
      GL_LINE,
      GL_POINT
  );
  private int polygonMode = polygonModes.get(0);

  private final List<Integer> topologies = Arrays.asList(
      GL_TRIANGLES,
      GL_TRIANGLE_STRIP
  );
  private int topology = topologies.get(0);

  private int colorMode = 0;
  private final int colorModes = 5; // počet módů (nechci dělat enum, nebo tak něco)

  private boolean orthogonal = false;
  private boolean displayLightDebug = false;
  private boolean lightAttenuation = false;
  private boolean lightCutoff = false;

  @Override
  public void init() {
    OGLUtils.printOGLparameters();
    OGLUtils.printJAVAparameters();
    OGLUtils.printLWJLparameters();
    OGLUtils.shaderCheck();

    startTime = System.nanoTime();
    currentTime = computeTime();

    glClearColor(0, 0, 0, 1);
    shaderProgramViewer = ShaderUtils.loadProgram("/start");
    shaderProgramLight = ShaderUtils.loadProgram("/light");

    locView = glGetUniformLocation(shaderProgramViewer, "view");
    locProjection = glGetUniformLocation(shaderProgramViewer, "projection");
    locSolid = glGetUniformLocation(shaderProgramViewer, "solid");
    locLightPosition = glGetUniformLocation(shaderProgramViewer, "lightPosition");
    locEyePosition = glGetUniformLocation(shaderProgramViewer, "eyePosition");
    locLightVP = glGetUniformLocation(shaderProgramViewer, "lightVP");
    locColorMode = glGetUniformLocation(shaderProgramViewer, "colorMode");
    locCameraPosition = glGetUniformLocation(shaderProgramViewer, "cameraPosition");

    locViewLight = glGetUniformLocation(shaderProgramLight, "view");
    locProjectionLight = glGetUniformLocation(shaderProgramLight, "projection");
    locSolidLight = glGetUniformLocation(shaderProgramLight, "solid");

    locTime = glGetUniformLocation(shaderProgramViewer, "time");
    locTimeLight = glGetUniformLocation(shaderProgramLight, "time");
    locLightAttenuation = glGetUniformLocation(shaderProgramViewer, "lightAttenuation");
    locLightCutoff = glGetUniformLocation(shaderProgramViewer, "lightCutoff");

    camera = new Camera()
        .withPosition(new Vec3D(-3, 3, 3))
        .withAzimuth(-1 / 4.0 * Math.PI)
        .withZenith(-1.3 / 5.0 * Math.PI);

    projection = getProjection();

    buffers = GridFactory.generateTriangleListGrid(150, 150);
    model1 = new OGLModelOBJ("/models/gordon.obj");
    model2 = new OGLModelOBJ("/models/dna.obj");
    renderTarget = new OGLRenderTarget(1024, 1024);
    viewer = new OGLTexture2D.Viewer();

    cameraLight = new Camera().withPosition(new Vec3D(5, 5, 5));
    cameraLight = cameraLight.withAzimuth(5 / 4f * Math.PI).withZenith(-1 / 5f * Math.PI);

    textRenderer = new OGLTextRenderer(width, height);

    try {
      mosaicTexture = new OGLTexture2D("textures/mosaic.jpg");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private Mat4 getProjection() {
    if (orthogonal) {
      return new Mat4OrthoRH(10, 7, 1, 20);
    }
    return new Mat4PerspRH(Math.PI / 3, 600 / 800f, 1.0, 20.0);
  }

  private OGLBuffers getGrid() {
    int size = 200;
    if (topology == GL_TRIANGLE_STRIP) {
      return GridFactory.generateTriangleStripGrid(size, size);
    }
    return GridFactory.generateTriangleListGrid(size, size);
  }

  private double computeTime() {
    return (System.nanoTime() - startTime) / 1000000000f;
  }

  @Override
  public void display() {
    glEnable(GL_DEPTH_TEST); // zapnout z-buffer (kvůli TextRendereru)

    update();
    glPolygonMode(GL_FRONT_AND_BACK, polygonMode);
    renderFromLight();
    renderFromViewer();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if (displayLightDebug) {
      viewer.view(renderTarget.getColorTexture(), -1, -1, 0.7);
      viewer.view(renderTarget.getDepthTexture(), -1, -0.3, 0.7);
    }

    textRenderer.addStr2D(width - 250, height - 25, "Tlasky's graphics playground.");
    textRenderer.addStr2D(width - 250, height - 10, "https://tlasky.herokuapp.com/");
  }

  private void update() {
    currentTime = computeTime();

    double direction = Math.sin(currentTime);
    if (direction < 0) {
      cameraLight = cameraLight.right(0.01);
    } else {
      cameraLight = cameraLight.left(0.01);
    }
  }

  private void renderFromLight() {
    glUseProgram(shaderProgramLight);
    renderTarget.bind();

    glClearColor(0.5f, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniformMatrix4fv(locViewLight, false, cameraLight.getViewMatrix().floatArray());
    glUniformMatrix4fv(locProjectionLight, false, projection.floatArray());

    glUniform1f(locTime, (float) currentTime);

    for (int i = 0; i < 7; i++) {
      glUniform1i(locSolid, i);
      buffers.draw(topology, shaderProgramViewer);
    }
    glUniform1i(locSolid, 7);
    model1.getBuffers().draw(model1.getTopology(), shaderProgramViewer);
    glUniform1i(locSolid, 8);
    model2.getBuffers().draw(model2.getTopology(), shaderProgramViewer);
  }

  private void renderFromViewer() {
    glUseProgram(shaderProgramViewer);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, width, height);

    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderTarget.getDepthTexture().bind(shaderProgramViewer, "depthTexture", 1);
    mosaicTexture.bind(shaderProgramViewer, "mosaic", 0);

    glUniform3fv(locLightPosition, ToFloatArray.convert(cameraLight.getPosition()));
    glUniform3fv(locEyePosition, ToFloatArray.convert(camera.getEye()));

    glUniformMatrix4fv(locView, false, camera.getViewMatrix().floatArray());
    glUniformMatrix4fv(locProjection, false, projection.floatArray());
    glUniformMatrix4fv(locLightVP, false, cameraLight.getViewMatrix().mul(projection).floatArray());

    glUniform1f(locTime, (float) currentTime);
    glUniform1i(locColorMode, colorMode);
    glUniform1i(locLightAttenuation, lightAttenuation ? 1 : 0);
    glUniform3fv(locCameraPosition, ToFloatArray.convert(camera.getPosition()));
    glUniform1i(locLightCutoff, lightCutoff ? 1 : 0);

    for (int i = 0; i < 7; i++) {
      glUniform1i(locSolid, i);
      buffers.draw(topology, shaderProgramViewer);
    }
    glUniform1i(locSolid, 7);
    model1.getBuffers().draw(model1.getTopology(), shaderProgramViewer);
    glUniform1i(locSolid, 8);
    model2.getBuffers().draw(model2.getTopology(), shaderProgramViewer);
  }

  @Override
  public GLFWCursorPosCallback getCursorCallback() {
    return cursorPosCallback;
  }

  @Override
  public GLFWMouseButtonCallback getMouseCallback() {
    return mouseButtonCallback;
  }

  @Override
  public GLFWKeyCallback getKeyCallback() {
    return keyCallback;
  }

  private final GLFWCursorPosCallback cursorPosCallback = new GLFWCursorPosCallback() {
    @Override
    public void invoke(long window, double x, double y) {
      if (mousePressed) {
        camera = camera.addAzimuth(Math.PI * (oldMx - x) / LwjglWindow.WIDTH);
        camera = camera.addZenith(Math.PI * (oldMy - y) / LwjglWindow.HEIGHT);
        oldMx = x;
        oldMy = y;
      }
    }
  };

  private final GLFWMouseButtonCallback mouseButtonCallback = new GLFWMouseButtonCallback() {
    @Override
    public void invoke(long window, int button, int action, int mods) {
      if (button == GLFW_MOUSE_BUTTON_LEFT) {
        double[] xPos = new double[1];
        double[] yPos = new double[1];
        glfwGetCursorPos(window, xPos, yPos);
        oldMx = xPos[0];
        oldMy = yPos[0];
        mousePressed = (action == GLFW_PRESS);
      }
    }
  };

  private final GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
      if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        switch (key) {
          case GLFW_KEY_A:
            camera = camera.left(0.1);
            break;
          case GLFW_KEY_D:
            camera = camera.right(0.1);
            break;
          case GLFW_KEY_W:
            camera = camera.forward(0.1);
            break;
          case GLFW_KEY_S:
            camera = camera.backward(0.1);
            break;
          case GLFW_KEY_R:
            camera = camera.up(0.1);
            break;
          case GLFW_KEY_F:
            camera = camera.down(0.1);
            break;
        }
      }

      if (action == GLFW_PRESS) {
        int index;
        switch (key) {
          case GLFW_KEY_P:
            index = (topologies.indexOf(topology) + 1) % topologies.size();
            topology = topologies.get(index);
            buffers = getGrid();
            System.out.printf("Topology: %d%n", topology);
            break;
          case GLFW_KEY_L:
            index = (polygonModes.indexOf(polygonMode) + 1) % polygonModes.size();
            polygonMode = polygonModes.get(index);
            System.out.printf("Polygon mode: %d%n", polygonMode);
            break;
          case GLFW_KEY_O:
            colorMode = (colorMode + 1) % colorModes;
            System.out.printf("Color mode: %d%n", colorMode);
            break;
          case GLFW_KEY_I:
            orthogonal = !orthogonal;
            projection = getProjection();
            System.out.printf("Projection: %s%n%s%n", projection.getClass().getName(), projection);
            break;
          case GLFW_KEY_K:
            displayLightDebug = !displayLightDebug;
            System.out.printf("Display light debug: %s%n", displayLightDebug);
            break;
          case GLFW_KEY_J:
            lightAttenuation = !lightAttenuation;
            System.out.printf("Light attenuation: %s%n", lightAttenuation);
            break;
          case GLFW_KEY_U:
            lightCutoff = !lightCutoff;
            System.out.printf("Light cutoff:%s%n", lightCutoff);
            break;
        }
      }
    }
  };
}
