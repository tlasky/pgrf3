package utils;

import lwjglutils.OGLBuffers;

public class GridFactory {

  /**
   * @param w počet vrcholů na řádku
   * @param h počet vrcholů ve sloupci
   * @return OGLBuffers
   */

  public static OGLBuffers generateTriangleListGrid(int w, int h) {
    float[] vb = new float[w * h * 2];
    int index = 0;

    for (int j = 0; j < h; j++) {
      float y = j / (float) (h - 1);
      for (int i = 0; i < w; i++) {
        float x = i / (float) (w - 1);
        vb[index++] = x;
        vb[index++] = y;
      }
    }

    int[] ib = new int[(w - 1) * (h - 1) * 2 * 3];
    int index2 = 0;

    for (int r = 0; r < h - 1; r++) {
      int offset = r * w;
      for (int c = 0; c < w - 1; c++) {
        ib[index2++] = offset + c;
        ib[index2++] = offset + c + 1;
        ib[index2++] = offset + c + w;
        ib[index2++] = offset + c + 1;
        ib[index2++] = offset + c + 1 + w;
        ib[index2++] = offset + c + w;
      }
    }

    OGLBuffers.Attrib[] attributes = {
        new OGLBuffers.Attrib("inPosition", 2) // 2 floats per vertex
    };
    return new OGLBuffers(vb, attributes, ib);
  }

  public static OGLBuffers generateTriangleStripGrid(int w, int h) {
    float[] vb = new float[w * h * 2];
    int index = 0;

    for (int j = 0; j < h; j++) {
      float y = j / (float) (h - 1);
      for (int i = 0; i < w; i++) {
        float x = i / (float) (w - 1);
        vb[index++] = x;
        vb[index++] = y;
      }
    }

    int[] ib = new int[((h - 1) * w * 2 + (h - 1) * 2)];
    int index2 = 0;
    boolean goRight = true;
    for (int r = 0; r < h - 1; r++) {
      int offset = r * w;

      int c;
      if (goRight) {
        for (c = 0; c < w; c++) {
          ib[index2++] = offset + c;
          ib[index2++] = offset + c + w;
        }
        if (r < h) {
          ib[index2++] = offset + c + w - 1;
          ib[index2++] = offset + c + w - 1;
        }
      } else {
        for (c = w - 1; c >= 0; c--) {
          ib[index2++] = offset + c + w;
          ib[index2++] = offset + c;
        }
        if (r < h) {
          ib[index2++] = offset + c + w + 1;
          ib[index2++] = offset + c + w + 1;
        }
      }
      goRight = !goRight;
    }

    OGLBuffers.Attrib[] attributes = {
        new OGLBuffers.Attrib("inPosition", 2) // 2 floats per vertex
    };
    return new OGLBuffers(vb, attributes, ib);
  }
}
