### Tlasky's graphics playground
Všechno by tam mělo být.
Jako bonus považuju přidání obj modelů. 

Jar-ko není potřeba, ne?

## Ovládání
- **W, A, S, D** - Klasika
- **Mouse 1** - Klasika

- **P** - Přepínání topologie
- **L** - Přepínání polygon módu (výplň)
- **O** - Přepínání barevného módu
- **I** - Přepínání projektce
- **K** - Zobrazení debugu světla
- **J** - Zpínání útlumu světla prostředím
- **U** - Zapínání ořezu světla
